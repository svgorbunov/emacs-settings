;; ===============================
;; Глобальные настройки оболочки
;; ===============================

(add-to-list 'load-path "~/.emacs.d/lisp/")
(add-to-list 'load-path "~/.emacs.d/lisp/color-theme")
(add-to-list 'load-path "~/.emacs.d/lisp/org-sync")

(menu-bar-mode 0)
;;(tool-bar-mode 0)
;;(w32-send-sys-command 61488)
(setq visible-bell 1)

(setq
   backup-by-copying t      ; don't clobber symlinks
   backup-directory-alist
    '(("." . "./.saves"))    ; don't litter my fs tree
   delete-old-versions t
   kept-new-versions 6
   kept-old-versions 2
   version-control t)       ; use versioned backups

;; Настройка табуляции
(global-set-key (kbd "TAB") 'self-insert-command)
(setq default-tab-width 2)

;; Отображения номера столбца
(custom-set-variables '(column-number-mode t))

;; iBuffer
(global-set-key [?\C-x ?\C-b] 'ibuffer)
 (setq ibuffer-saved-filter-groups
          (quote (("default"
                   ("C#" (mode . csharp-mode))
                   ("Ruby" (mode . ruby-mode))
                   ("emacs" (or
                             (name . "^\\*scratch\\*$")
                             (name . "^\\*Completions\\*$")
				                     (name . "^\\*Compile-Log\\*$")
                             (name . "^\\*scratch\\*$")
                             (name . "^\\*Messages\\*$")))
                   ("tools" (or
                            (mode . dired-mode)
                            (mode . org-mode)
                            (name . "^.*hg-status.*$")
                            (name . "^\\*shell\\*.*")))))))
    (add-hook 'ibuffer-mode-hook
              (lambda ()
                (ibuffer-switch-to-saved-filter-groups "default")))


;; Настройка цветовой схемы
(require 'color-theme)
(color-theme-initialize)
(color-theme-sitaramv-nt) ;; (color-theme-scintilla)

;; Настройка номеров строк
(custom-set-variables
 '(inhibit-startup-screen t)
 '(linum-format " %d "))
(global-linum-mode 1)
(provide 'init-linum)

;; Подсветка текущей строки
;;(require 'highlight-current-line)
;;(highlight-current-line-on t)

;; Автозавершение
(require 'auto-complete-config)
(add-to-list 'ac-dictionary-directories "~/.emacs.d/lisp/ac-dict")
(ac-config-default)

;; Mercurial
(require 'ahg)

;; Rake
(setq auto-mode-alist (append '(("\[Rr]akefile\.*.*$" . ruby-mode)) auto-mode-alist))

;; Org Mode
(setq org-todo-keywords
       '((sequence "TODO(t)" "IN WORK(w)" "|" "DONE(d)")))
(setq org-log-done 'time)
(setq org-todo-keyword-faces
           '(("TODO" . org-warning) ("IN WORK" . "brown")))

(defun org-summary-todo (n-done n-not-done)
    "Switch entry to DONE when all subentries are done, to TODO otherwise."
(let (org-log-done org-log-states)   ; turn off logging
  (org-todo (if (= n-not-done 0) "DONE" "TODO"))))
     
(add-hook 'org-after-todo-statistics-hook 'org-summary-todo)
(setq org-clock-persist 'history)
(org-clock-persistence-insinuate)

(mapc 'load '("org-element" "os" "os-bb" "os-github" "os-rmine"))


;; YaSnippets
;;(add-to-list 'load-path "~/.emacs.d/lisp/yasnippet")
;;(require 'yasnippet)
;; (yas/global-mode 1) ;; - вставка табуляций ломается


;; ============================
;; Настройки для работы с .NET
;; ============================

;; TODO: 
;; 1) Flymake + StyleCop (csharp-mode по полной программе, возможно шаблоны файлов)
;; 2) Snippets
;; 3) Настроить отступы
;; 4) Настроить auto-complete
;; 5) Цвет выделения текущей строки
;; 6) Вертикальная граница кода
;; 7) Вынести настройки для дотнета в отдельный файл
;; 8) Сохранение сессий
;; 9) Работа с меркуриалом
;; 10) ОСВОИТЬ Org-Mode для TDD!!!!

(autoload 'csharp-mode "csharp-mode" "Главный режим для C#" t)
(setq auto-mode-alist
      (append '(("\\.cs$" . csharp-mode)) auto-mode-alist))
(add-hook 'charp-mode-hook '(auto-complete-mode))

(defun my-c-mode-common-hook ()
  (setq c-basic-offset 2)
  (c-set-offset 'substatement-open 0))
(add-hook 'c-mode-common-hook 'my-c-mode-common-hook)
;; (add-hook 'c-mode-common-hook '(lambda () (c-set-style "BSD")))

(put 'dired-find-alternate-file 'disabled nil)

(require 'sr-speedbar)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(sr-speedbar-auto-refresh nil)
 '(sr-speedbar-right-side nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

